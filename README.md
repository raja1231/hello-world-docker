# Hello world in docker
This is an example project showing how to build an application in a container using GitLab CI.  We are building a container which runs our python application. The python application also exectutes my compiled C program.  

GitlabCI will:
-  build the app in a container
-  run the container
-  push the container into this projects container registry
-  run the testscript.sh inside the built container
-  scan the container for vulnerabilities
-  deploy the container somewhere

## File descriptions  
**Dockerfile**  
builds my application into a container.  
**testscript.sh**  
A simple bash script to perform some tests.  
**.gitlab-ci.yml**  
This is the gitlab-ci pipeline which builds, tests,and deploys the application.  
**mypythonapp.py**  
This is my python application.  
**hello.c**  
This is my C application  
**bad-code-examples**  
Some examples of bad code to test vulnerability scanning.  

## How to build this project locally
1. clone the repo
```
git clone https://gitlab.com/keeneproject/example-projects/hello-world-docker.git
```
2. build the container and run it
```
cd hello-world-docker
docker build -t helloworld-app .
docker run --rm helloworld-app:latest
```

## Useful links
https://docs.docker.com/get-started/
https://docs.gitlab.com/ee/ci/quick_start/

